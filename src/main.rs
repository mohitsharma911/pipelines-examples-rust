//! A web service for matching patterns from a noisy signal against a dictionary of known words from
//! an [alien language][1].
//!
//! ```text
//! $ cargo run &
//! Listening { socket: V4(0.0.0.0:8080) }
//!
//! $ curl -X POST localhost:8080 --data-binary @input/sample.txt
//! Case #1: 2
//! Case #2: 1
//! Case #3: 3
//! Case #4: 0
//! ```
//!
//! [1]: https://code.google.com/codejam/contest/90101/dashboard#s=p0

#![deny(warnings, missing_docs)]

use std::io::prelude::*;
use std::io::BufReader;

extern crate hyper;
use hyper::header::ContentType;
use hyper::mime::{Attr, Mime, SubLevel, TopLevel, Value};
use hyper::server::{Request, Response, Server};
use hyper::status::StatusCode;

extern crate alienlanguage;
use alienlanguage::Problem;

fn handler(req: Request, mut res: Response) {
    if req.method != hyper::Post {
        *res.status_mut() = StatusCode::MethodNotAllowed;
        return;
    }
    let mut lines = BufReader::new(req).lines();
    let problem = Problem::from_lines(&mut lines);
    res.headers_mut().set(ContentType(Mime(
        TopLevel::Text,
        SubLevel::Plain,
        vec![(Attr::Charset, Value::Utf8)],
    )));
    let mut res = res.start().unwrap();
    for case in problem {
        res.write_all(case.as_bytes()).unwrap();
        res.write_all(b"\n").unwrap();
    }
}

fn main() {
    let server = Server::http("0.0.0.0:8080").unwrap();
    let listening = server.handle(handler).unwrap();
    println!("{:?}", listening);
}
